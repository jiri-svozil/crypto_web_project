import re

def clean_response(data):
    decoded = data.decode()
    semifinal = re.findall("=\w+", decoded)
    final = list()
    for i in range(len(semifinal)):
        final.append(int(re.sub("=", "", semifinal[i])) - 1)

    return final