import pyotp

from md.cl.user import *
from md._db.select import Select
import bcrypt

import re

def verify_password(client_password, database_user: User):
    if bcrypt.checkpw(client_password.encode('utf-8'), (database_user.hash_password).encode('utf-8')):
        return True
    else:
        return False

def verify_form(form):

    users = Select.users().getUserNames()

    errors = {"username_error": "", "same_password": "", "email_form": "", "else": ""}


    if form["username"] in users:
        errors["username_error"] = "Username {} is already exist.".format(form["username"])

    elif form["password"] != form["c_password"]:
        errors["same_password"] = "Passwords must be same."

    elif checkMailValidity(form["email"]) == False:
        errors["email_form"] = "Email has wrong form."

    elif form["first_name"] == "" or form["surname"] == "" or form["password"] == "":
        errors["else"] = "Form must have full."

    else:
        errors = False



    return errors




def checkMailValidity(inputEmail: str):
    pattern = r"([\w\.-]+)@([\w\.-]+)(\.[\w\.]+)"
    if re.search(pattern, inputEmail) == None:
        return False
    else:
        return True

def verifyPasswordConfirm(form):
    errors = {"same_password": ""}
    if form["new_password"] != form["c_new_password"]:
        errors["same_password"] = "Passwords must be same."
    else:
        errors = False

    return errors

def verifyTwoFactor(OTP, database_user: User):
    if pyotp.TOTP(database_user.two_factor).now() == OTP:
        return True
    else:
        return False



