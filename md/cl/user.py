class User:
    def __init__(self, user_id, username, name, surname, hash_password, two_factor, login, email, user_type):
        self.user_id = user_id
        self.username = username
        self.name = name
        self.surname = surname
        self.two_factor = two_factor
        self.login = login
        self.hash_password = hash_password
        self.email = email
        self.user_type = user_type

    def getUserInfo(self):
        user = {
            "user_id": self.user_id,
            "username": self.username,
            "name": self.name,
            "surname": self.surname,
            "email": self.email,
            "user_type": self.user_type
        }
        return user



class List_of_user:
    def __init__(self):
        self.main_list = list()

    def addUser(self, user):
        self.main_list.append(user)

    def getUserNames(self):
        user_names = list()
        for i in range(len(self.main_list)):
            user_names.append(self.main_list[i].username)

        return user_names

    def getUser(self, username):
        for i in range(len(self.main_list)):
            if username == self.main_list[i].username:
                myuser = self.main_list[i]
                break
        return myuser

    def getUsersInfo(self):
        users = list()
        for i in range(len(self.main_list)):
            user = {
                "user_id": self.main_list[i].user_id,
                "username": self.main_list[i].username,
                "name": self.main_list[i].name,
                "surname": self.main_list[i].surname,
                "email": self.main_list[i].email,
                "user_type": self.main_list[i].user_type
                    }
            users.append(user)

        return users
