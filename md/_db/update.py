from md.cl.user import *
from md._db.connection import Connection


class Update:

    @staticmethod
    def user_password(new_user: User):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'UPDATE user SET password=%s WHERE user_id=%s;'
        ar = (new_user.hash_password, new_user.user_id, )
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def user_emial(new_user: User):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'UPDATE user SET email=%s WHERE user_id=%s;'
        ar = (new_user.email, new_user.user_id,)
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def user_type(new_user: User):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'UPDATE user SET user_type_id=(SELECT user_type_id FROM user_type WHERE type=%s) WHERE user_id=%s;'
        ar = (new_user.user_type, new_user.user_id,)
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def setup_two_factor(new_user: User):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'UPDATE user SET two_factor = %s WHERE user_id = %s;'
        ar = (new_user.two_factor, new_user.user_id,)
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def login(new_user: User):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'UPDATE user SET login = %s WHERE user_id = %s;'
        ar = (new_user.login, new_user.user_id,)
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()