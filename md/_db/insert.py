from md.cl.records import *
from md.cl.user import *
from md._db.connection import Connection

class Insert:


    @staticmethod
    def add_data(username, datatype, data):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        if data["date"] == "":
            query = 'INSERT INTO data(value, date, user_id, data_type_id) VALUES(%s, now(), (SELECT user_id FROM user WHERE username=%s), (SELECT data_type_id FROM data_type WHERE type=%s));'
            ar = (data["value"], username, datatype,)
        elif data["time"] == "" and data["date"] != "":
            date = "{} {}".format(data["date"], "00:00")
            query = 'INSERT INTO data(value, date, user_id, data_type_id) VALUES(%s, %s, (SELECT user_id FROM user WHERE username=%s), (SELECT data_type_id FROM data_type WHERE type=%s));'
            ar = (data["value"], date, username, datatype,)
        else:
            date = "{} {}".format(data["date"], data["time"])
            query = 'INSERT INTO data(value, date, user_id, data_type_id) VALUES(%s, %s, (SELECT user_id FROM user WHERE username=%s), (SELECT data_type_id FROM data_type WHERE type=%s));'
            ar = (data["value"], date, username, datatype,)

        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def user(new_user: User):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'INSERT INTO user(username, name, surname, password, email, user_type_id) VALUES(%s, %s, %s, %s, %s, (SELECT user_type_id FROM user_type WHERE type=%s));'
        ar = (new_user.username, new_user.name, new_user.surname, new_user.hash_password, new_user.email, new_user.user_type)
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True
