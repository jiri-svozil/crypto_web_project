from md.cl.records import *
from md.cl.user import *
from md._db.connection import Connection

class Delete:
    @staticmethod
    def record_from_data(id):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = 'DELETE FROM data WHERE data_id=%s'
        ar = (id,)
        mycursor.execute(query, ar)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def data(list_of_ids):
        for i in range(len(list_of_ids)):
            Delete.record_from_data(list_of_ids[i])
        return True


    @staticmethod
    def data_by_ids(list_of_del, database_data):
        list_of_data = database_data.getRecords()
        ids = list()
        for i in range(len(list_of_del)):
            ids.append(list_of_data[list_of_del[i]]["id"])
            list_of_data.pop(list_of_del[i])

        if len(ids) > 0:
            Delete.data(ids)
        return True


    @staticmethod
    def user(user_id: int):
        conn = Connection()
        connection = conn.get_connection()
        mycursor = connection.cursor()
        query = "DELETE FROM user WHERE user_id=%s"
        adr = (user_id, )
        mycursor.execute(query, adr)
        connection.commit()
        conn.close_connection()
        return True

    @staticmethod
    def users(list_of_user_ids: list):
        for i in range(len(list_of_user_ids)):
            Delete.user(list_of_user_ids[i])

        return True

    @staticmethod
    def users_by_user_id(list_of_del, database_users):
        list_of_users = database_users.getUsersInfo()
        list_of_users.reverse()
        ids = list()
        for i in range(len(list_of_del)):
            ids.append(list_of_users[list_of_del[i]]["user_id"])
            list_of_users.pop(list_of_del[i])

        if len(ids) > 0:
            Delete.users(ids)

        return True


