from mysql.connector import *
from db_config import DATABASE

class Connection:
    def __init__(self):
        self.dbConnection = None


    def get_connection(self):
        self.dbConnection = connect(
            host=DATABASE["HOST"],
            user=DATABASE["USER"],
            password=DATABASE["PASSWORD"],
            database=DATABASE["DATABASE"]
        )
        return self.dbConnection

    def close_connection(self):
        self.dbConnection.close()



