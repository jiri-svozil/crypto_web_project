from md.cl.records import *
from md.cl.user import *
from md._db.connection import Connection

class Select:
    @staticmethod
    def users():
        users = List_of_user()
        conn = Connection()
        my_data = conn.get_connection()
        mycursor = my_data.cursor()

        mycursor.execute("SELECT * from private_user")

        myresult = mycursor.fetchall()

        for i in myresult:
            user = User(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8])
            users.addUser(user)

        conn.close_connection()
        return users

    @staticmethod
    def get_data(username, data_type):
        data = List_of_records()
        conn = Connection()
        my_data = conn.get_connection()
        mycursor = my_data.cursor()
        query = 'SELECT data_id, value, date, user_id, type FROM data_with_type WHERE user_id=(SELECT user_id FROM user WHERE username=%s) and type=%s ORDER BY date;'
        ar = (username, data_type, )

        mycursor.execute(query, ar)


        myresult = mycursor.fetchall()
        for i in myresult:
            one_record = Record(i[0], i[1], i[2], i[3], i[4])

            data.addRecord(one_record)
        conn.close_connection()
        return data


    @staticmethod
    def user_by_username(username):
        conn = Connection()
        my_data = conn.get_connection()
        mycursor = my_data.cursor()
        query = 'SELECT * from private_user WHERE username=%s'
        ar = (username, )

        mycursor.execute(query, ar)

        myresult = mycursor.fetchall()

        for i in myresult:
            user = User(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8])

        conn.close_connection()
        return user

    @staticmethod
    def user_by_login(login):
        conn = Connection()
        my_data = conn.get_connection()
        mycursor = my_data.cursor()
        query = 'SELECT * from private_user WHERE login=%s'
        ar = (login,)

        mycursor.execute(query, ar)

        myresult = mycursor.fetchall()

        for i in myresult:
            user = User(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8])

        conn.close_connection()
        return user

