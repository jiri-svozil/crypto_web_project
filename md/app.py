

from flask import Flask, render_template, request, session, redirect, url_for
from md.cl.user import *
from md.cl.verify import *
from md.cl.text_work import *
from md._db.select import Select
from md._db.insert import Insert
from md._db.delete import Delete
from md._db.update import Update

import bcrypt

from datetime import datetime
import hashlib
import pyotp


flask_app = Flask(__name__)
flask_app.secret_key = b'P{\xde\xcdUD^\xb1-\x16K\x06\x8e\xe0\x04\xab\xd3IY\xf5O\xb9\xb5~'





@flask_app.route('/')
def welcome_page():
    return render_template("welcome_page.jinja2")

@flask_app.route('/about')
def page_about():
    return render_template('about.jinja2')

@flask_app.route('/login/', methods=['POST'])
def login_user():
    my_list = Select.users()
    user_names = my_list.getUserNames()
    username = request.form["username"]
    password = request.form["password"]
    if username in user_names:
        my_user = my_list.getUser(username)
        result = verify_password(password, my_user)
        if result == True:
            user_by_username = Select.user_by_username(username)
            if user_by_username.two_factor == None:
                session["logged"] = True
                session["user"] = my_user.username
                session["user_type"] = my_user.user_type
                return redirect(url_for("account"))
            else:
                my_user.login = str(hashlib.sha256(str(datetime.now()).encode()).hexdigest())
                Update.login(my_user)
                return redirect(url_for('two_factor_authentication', login=(my_user.login)))
        else:
            return redirect(url_for("login_page"))
    else:
        return redirect(url_for("login_page"))

@flask_app.route("/login/", methods=["GET"])
def login_page():
    return render_template("login.jinja2")

@flask_app.route("/login/two_factor/", methods=["POST"])
def two_factor_authentication():
    OTP = request.form["otp"]
    url = request.form["url"]
    url_split = url.split("=")
    login = url_split[1]
    user_by_login = Select.user_by_login(login)
    user_by_login.login = None
    Update.login(user_by_login)
    if verifyTwoFactor(OTP, user_by_login):
        my_list = Select.users()
        my_user = my_list.getUser(user_by_login.username)
        session["logged"] = True
        session["user"] = my_user.username
        session["user_type"] = my_user.user_type
        return redirect(url_for("account"))
    else:
        return redirect(url_for("login_page"))

@flask_app.route("/login/two_factor/", methods=["GET"])
def two_factor_authentication_page():
    return render_template("two_factor_authentication.jinja2")

@flask_app.route('/register/', methods=["POST"])
def register_user():
    errors = verify_form(request.form)
    if errors == False:
        hash_password = bcrypt.hashpw((request.form["password"]).encode('utf-8'), bcrypt.gensalt()) #"Hashed password" function(request.form["password"])
        new_user = User(None, request.form["username"], request.form["first_name"], request.form["surname"], hash_password.decode(encoding='utf-8'),  None, None, request.form["email"], "normal_user")
        Insert.user(new_user)
        return redirect(url_for("register_successful"))
    return render_template('register.jinja2', errors=errors)

@flask_app.route('/register/done', methods=["GET"])
def register_successful():
    return render_template('register_successful.jinja2')


@flask_app.route('/register/', methods=["GET"])
def register_page():
    return render_template('register.jinja2', errors="")


@flask_app.route('/login_or_register/', methods=["GET"])
def log_or_reg():
    return render_template('login_or_register.jinja2')

@flask_app.route('/settings/', methods=["GET"])
def settings():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    user = Select.user_by_username(session["user"])
    return render_template('settings/settings_main.jinja2', data=user.getUserInfo())

@flask_app.route('/settings/password', methods=["GET"])
def settings_password():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    return render_template('settings/settings_password.jinja2', errors="")

@flask_app.route('/settings/password', methods=["POST"])
def change_password():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    user = Select.user_by_username(session["user"])
    result = verify_password(request.form["old_password"], user)
    errors = verifyPasswordConfirm(request.form)
    if result == True:
        error = ""
        if errors == False:
            hash_password = bcrypt.hashpw((request.form["new_password"]).encode('utf-8'), bcrypt.gensalt()) #request.form["new_password"]   function(request.form["new_password"])
            user.hash_password = hash_password
            Update.user_password(user)
            return redirect(url_for("password_successful"))
    else:
        error = "Wrong password"
    return render_template('settings/settings_password.jinja2', errors=errors, error=error)

@flask_app.route('/settings/password/done', methods=["GET"])
def password_successful():
    return render_template('settings/password_successful.jinja2')

@flask_app.route('/settings/email', methods=["GET"])
def settings_email():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    return render_template('settings/settings_email.jinja2')

@flask_app.route('/settings/email', methods=["POST"])
def change_email():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    user = Select.user_by_username(session["user"])
    result = verify_password(request.form["password"], user)
    if result == True:
        user.email = request.form["new_email"]
        Update.user_emial(user)
        return redirect(url_for("email_successful"))
    else:
        error = "Wrong password"
    return render_template('settings/settings_email.jinja2', error=error)

@flask_app.route('/settings/email/done', methods=["GET"])
def email_successful():
    return render_template('settings/email_successful.jinja2')

@flask_app.route('/settings/two_factor_verification_setup', methods=["GET"])
def two_factor_verification_setup():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    return render_template('settings/settings_two_factor_verification_setup.jinja2')

@flask_app.route('/settings/two_factor_qr', methods=["GET"])
def two_factor_qr():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    s = str(hashlib.sha256(str(datetime.now()).encode()).hexdigest())
    s16 = ""  # 2-7, a-z
    x = 0
    while len(s16) < 16:
        if not (s[x] == '0' or s[x] == '1' or s[x] == '8' or s[x] == '9'):
            s16 += s[x]
        x += 1
    t = pyotp.TOTP(s16)
    user = Select.user_by_username(session["user"])
    user.two_factor = s16
    Update.setup_two_factor(user)

    return render_template('settings/two_factor_qr.jinja2', data=s16)

@flask_app.route('/settings/user_type', methods=["GET"])
def settings_user_type():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    return render_template('settings/settings_user_type.jinja2')

@flask_app.route('/settings/user_type', methods=["POST"])
def change_user_type():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    user = Select.user_by_username(session["user"])
    if request.form["submit"] == "To normal":
        user.user_type = "normal_user"
        Update.user_type(user)
        session["user_type"] = Select.user_by_username(session["user"]).user_type
        return redirect(url_for("user_type_successful"))
    if request.form["submit"] == "To premium":
        user.user_type = "premium_user"
        Update.user_type(user)
        session["user_type"] = Select.user_by_username(session["user"]).user_type
        return redirect(url_for("user_type_successful"))
    return render_template('settings/settings_user_type.jinja2')

@flask_app.route('/settings/user_type/done', methods=["GET"])
def user_type_successful():
    return render_template('settings/user_type_successful.jinja2')


@flask_app.route('/account/', methods=["GET"])
def account():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    return render_template('my_acount_page.jinja2')

@flask_app.route('/account/users', methods=["GET"])
def admin_users():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    users = Select.users()
    users_info = users.getUsersInfo()

    return render_template("admin_users.jinja2", data=users_info, len=len(users_info))

@flask_app.route('/account/users/delete', methods=["POST"])
def delete_users():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    list_of_del = clean_response(request.get_data())
    Delete.users_by_user_id(list_of_del, Select.users())

    return redirect(url_for("admin_users"))


@flask_app.route('/account/height', methods=["GET"])
def height():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    moje_data = Select.get_data(session["user"], "Height")
    mylist = moje_data.getRecords()
    mylist.reverse()
    return render_template('height.jinja2', data=mylist, len=len(mylist))

@flask_app.route('/account/height', methods=["POST"])
def add_height():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    data = {"value": request.form["value"], "date": request.form["date"], "time": request.form["time"]}
    Insert.add_data(session["user"], "Height", data)
    return redirect(url_for("height"))

@flask_app.route('/account/height/delete', methods=["POST"])
def delete_height():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    list_of_del = clean_response(request.get_data())
    Delete.data_by_ids(list_of_del, Select.get_data(session["user"], "Height"))
    return redirect(url_for("height"))

@flask_app.route('/account/weight', methods=["GET"])
def weight():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    moje_data = Select.get_data(session["user"], "Weight")
    mylist = moje_data.getRecords()
    mylist.reverse()
    return render_template('weight.jinja2', data=mylist, len=len(mylist))

@flask_app.route('/account/weight', methods=["POST"])
def add_weight():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    data = {"value": request.form["value"], "date": request.form["date"], "time": request.form["time"]}
    Insert.add_data(session["user"], "Weight", data)
    return redirect(url_for("weight"))

@flask_app.route('/account/weight/delete', methods=["POST"])
def delete_weight():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))
    list_of_del = clean_response(request.get_data())
    Delete.data_by_ids(list_of_del, Select.get_data(session["user"], "Weight"))
    return redirect(url_for("weight"))

@flask_app.route('/account/oxygen', methods=["GET"])
def oxygen():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    moje_data = Select.get_data(session["user"], "Oxygen")
    mylist = moje_data.getRecords()
    mylist.reverse()
    return render_template('oxygen.jinja2', data=mylist, len=len(mylist))

@flask_app.route('/account/oxygen', methods=["POST"])
def add_oxygen():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    data = {"value": request.form["value"], "date": request.form["date"], "time": request.form["time"]}
    Insert.add_data(session["user"], "Oxygen", data)
    return redirect(url_for("oxygen"))

@flask_app.route('/account/oxygen/delete', methods=["POST"])
def delete_oxygen():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    list_of_del = clean_response(request.get_data())
    Delete.data_by_ids(list_of_del, Select.get_data(session["user"], "Oxygen"))
    return redirect(url_for("oxygen"))

@flask_app.route('/account/kcal', methods=["GET"])
def kcal():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    moje_data = Select.get_data(session["user"], "KCAL")
    mylist = moje_data.getRecords()
    mylist.reverse()
    return render_template('kcal.jinja2', data=mylist, len=len(mylist))

@flask_app.route('/account/kcal', methods=["POST"])
def add_kcal():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    data = {"value": request.form["value"], "date": request.form["date"], "time": request.form["time"]}
    Insert.add_data(session["user"], "KCAL", data)
    return redirect(url_for("kcal"))

@flask_app.route('/account/kcal/delete', methods=["POST"])
def delete_kcal():
    if "logged" not in session:
        return redirect(url_for("log_or_reg"))

    list_of_del = clean_response(request.get_data())
    Delete.data_by_ids(list_of_del, Select.get_data(session["user"], "KCAL"))
    return redirect(url_for("kcal"))

@flask_app.route('/logout/', methods=["POST"])
def logout():
    session.pop("logged")
    return redirect(url_for("welcome_page"))





