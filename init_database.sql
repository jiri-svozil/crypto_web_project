create database if not exists LiFit_database;
use LiFit_database;

create table if not exists user_type (
	user_type_id	int	not null	auto_increment,
  type  varchar(45) not null,
  primary key(user_type_id)
);

create table if not exists data_type (
	data_type_id	int	not null	auto_increment,
  type  varchar(45) not null,
  primary key(data_type_id)
);

create table if not exists user (
	user_id int	not null	auto_increment,
	username varchar(45) not null,
  name	varchar(45)	not null,
  surname	varchar(45)	not null,
	password varchar(90)	not null,
  email	varchar(45),
	two_factor varchar(45),
	login varchar(90),
	user_type_id int	not null,
  primary key(user_id),
	unique(username),
	foreign key(user_type_id) references user_type(user_type_id)
);


create table if not exists data (
	data_id	int	not null auto_increment,
  value varchar(45) not null,
	date DATETIME not null,
	user_id int not null,
	data_type_id int not null,
  primary key(data_id),
  foreign key(user_id) references user(user_id) ON DELETE CASCADE,
	foreign key(data_type_id) references data_type(data_type_id)
);


CREATE VIEW private_user AS
SELECT u.user_id, u.username, u.name, u.surname, u.password, u.two_factor, u.login, u.email, t.type FROM user u
JOIN user_type t
ON u.user_type_id = t.user_type_id;


CREATE VIEW data_with_type AS
SELECT d.data_id, d.value, d.date, d.user_id, t.type FROM data d
JOIN data_type t
ON t.data_type_id = d.data_type_id;


INSERT INTO user_type(type) VALUES ('admin');
INSERT INTO user_type(type) VALUES ('normal_user');
INSERT INTO user_type(type) VALUES ('premium_user');


INSERT INTO data_type(type) VALUES ('Height');
INSERT INTO data_type(type) VALUES ('Weight');
INSERT INTO data_type(type) VALUES ('Oxygen');
INSERT INTO data_type(type) VALUES ('KCAL');

INSERT INTO user(username, name, surname, email, password, user_type_id) VALUES("admin", "Admin", "Adminovič","admin@admin.cz","$2b$12$gws22QhxWRoTr/Tm6k1m9et9zqqlhn7HFVyFykFGEAGT2ekjMSsz6", 1);
