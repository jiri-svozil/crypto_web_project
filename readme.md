# Spuštění aplikace se je možno provést Následujícimi způsoby:

### A) Z příkazové řádky (bez Dockeru):
1) Vytvoření a inicializace databáze - do nainstalované mysql databáze je nutné nahrát databázi - To se provede skopírováním obsahu init.sql a následným vložením do databáze (např. v MysqlWorkbranch)
2) Nastavení databáze v souboru db_config.py - zde je nutné zadat údaje o databázi (tj. ip adresa, username, heslo, schema)
3) Vygenerování klíču (tento bod je nepovinný...) - z bezpečnostních důvoduů je nutné v adresáři keys vygenerovat klíče pro HTTPS komunikaci:
   1) Vymazání defaultních klíčů...
   2) Vygenerování nových: `openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365`
4) Doinstalování potřebných knihoven - používali jsme několik externích knihoven proto je nutné je doinstalovat<br>
    1) Instalace Flasku - `pip3 install Flask`
   2) Instalace knihovny bcrypt - `pip3 install bcrypt`
   3) Instalace knihovny pyotp - `pip3 install pyotp`
   4) Instalace knihovny pyopenssl - `pip3 install pyopenssl`
   5) Instalace knihovny mysql-connector-python - `pip3 install mysql-connector-python`
   6) Instalace knihovny datetime - `pip3 install datetime`
   
5) Spuštení aplikace: python3 run.py

### B) Pomocí Dockeru:
Je nutné mít správně nainstalovaný Docker...
1) Vytvoření a inicializace databáze - do nainstalované mysql databáze je nutné nahrát databázi - To se provede skopírováním obsahu init.sql a následným vložením do databáze (např. v MysqlWorkbranch)
2) Nastavení databáze v souboru db_config.py - zde je nutné zadat údaje o databázi (tj. ip adresa, username, heslo, schema)
3) Vygenerování klíču (tento bod je nepovinný...) - z bezpečnostních důvoduů je nutné v adresáři keys vygenerovat klíče pro HTTPS komunikaci:
   1) Vymazání defaultních klíčů...
   2) Vygenerování nových: `openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365`
4) Vytvořit image: 
   `docker build --tag lifit_image crypto_web_project`
5) Spustění konteineru: <br>
   a) `docker run -p 443:443 --name lifit_server lifit_image` <br>
   b) `docker run -p -d 443:443 --name lifit_server lifit_image` (skryté spuštění...)
   

### B) Pomocí docker-compose:
Je nutné mít správně nainstalovaný Docker a docker-compose...
1) Nastavení databáze v souboru db_config.py - zde je nutné zadat údaje o databázi (tj. ip adresa, username, heslo, schema)
2) Vygenerování klíču (tento bod je nepovinný...) - z bezpečnostních důvoduů je nutné v adresáři keys vygenerovat klíče pro HTTPS komunikaci:
   1) Vymazání defaultních klíčů...
   2) Vygenerování nových: `openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365`
3) V adresáři crypto_web_project spustit: `docker-compose up` porpřípadně `docker-compose up -d` 
4) příkaz pro zastavení: `docker-compose stop` a pro nové spuštění: `docker-compose start`
   
   

    