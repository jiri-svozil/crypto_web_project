from md.app import flask_app

# you need install pyopenssl (pip install pyopenssl)

if __name__ == '__main__':
    keys = ('keys/cert.pem', 'keys/key.pem')
    flask_app.run(host='0.0.0.0', port=443, ssl_context=keys)